package id.tp3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.tp3.Helper.MenuDBHelper;

public class UpdateActivity extends AppCompatActivity {

    private EditText etNama;
    private EditText etTingkat;
    private Button btAdd;

    private MenuDBHelper dbHelper;
    private long receivedMenuId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        /*initialize*/
        etNama= findViewById(R.id.etNama);
        etTingkat= findViewById(R.id.etTingkat);
        btAdd= findViewById(R.id.btAdd);

        dbHelper= new MenuDBHelper(this);

        try{
            /*get intent to get menu id*/
            receivedMenuId= getIntent().getLongExtra("USER_ID", 1);
        }catch (Exception e){
            e.printStackTrace();
        }

        /*populate user data before update*/
        id.tp3.model.Menu quiredMenu= dbHelper.getMenu(receivedMenuId);
        /*set field to this user data*/
        etNama.setText(quiredMenu.getNama());
        etTingkat.setText(quiredMenu.getTingkatan());

        /*listen to add button click to update*/
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePerson();
            }
        });
    }

    private void updatePerson() {
        String nama= etNama.getText().toString().trim();
        String tingkat= etTingkat.getText().toString().trim();
//        String Image= etImage.getText().toString().trim();
        dbHelper= new MenuDBHelper(this);

        if(nama.isEmpty() || tingkat.isEmpty()){
            Toast.makeText(this, "Tolong lengkapi terlebih dahulu", Toast.LENGTH_SHORT).show();
        }

        id.tp3.model.Menu updateMenu= new id.tp3.model.Menu(nama, tingkat);

        dbHelper.updateMenuREcord(receivedMenuId, this, updateMenu);

        goBackHome();
    }

    public void goBackHome(){
        startActivity(new Intent(this, AdminActivity.class));
    }


    /*inflate icon/button yang ada di custom appbar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create2, menu);
        return  true;
    }
}
