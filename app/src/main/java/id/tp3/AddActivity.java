package id.tp3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.tp3.Helper.MenuDBHelper;

public class AddActivity extends AppCompatActivity {

    private EditText etNama;
    private EditText etTingkatan;
    private EditText etImage;
    private Button btAdd;

    private MenuDBHelper dbHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        /*inisialisasi toolbar*/
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar.setNavigationIcon(R.drawable.ic_toolbar);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");


        etNama= findViewById(R.id.etNama);
        etTingkatan= findViewById(R.id.etTingkat);
//        final EditText etImage= findViewById(R.id.etImage);
        btAdd= findViewById(R.id.btAdd);

//        listen to add button click
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMenu();
            }
        });
    }

    private void addMenu() {
        String nama= etNama.getText().toString().trim();
        String tingkat= etTingkatan.getText().toString().trim();
//        String Image= etImage.getText().toString().trim();
        dbHelper= new MenuDBHelper(this);

        if(nama.isEmpty() || tingkat.isEmpty()){
            Toast.makeText(this, "Tolong lengkapi terlebih dahulu", Toast.LENGTH_SHORT).show();
        }

        /*create new menu*/
        id.tp3.model.Menu menu= new id.tp3.model.Menu(nama, tingkat);
        Log.i("fk", String.valueOf(menu));
        dbHelper.saveNewMenu(menu);

        goBackHome();
    }

    public void goBackHome(){
        startActivity(new Intent(this, AdminActivity.class));
    }

    /*inflate icon/button yang ada di custom appbar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create2, menu);
        return  true;
    }
}
