package id.tp3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.session.MediaSessionManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.tp3.session.SessionManagement;

public class LoginActivity extends AppCompatActivity {

    /*Session manager class*/
//    SessionManagement session;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";
    SharedPreferences session;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*inisialitation session manager*/
//        session = new SessionManagement(getApplicationContext());
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//        String sessionTest= session.getString("Nama", "");

        Toast.makeText(getApplicationContext(), session.getString("Name", ""), Toast.LENGTH_SHORT).show();
        /*check session login*/
        String user = session.getString("Name", "");
//        String email = sp.getString("email", "");
        boolean logged = session.getBoolean("logged", false);
        if (!user.equals("") && logged) {
            Log.i("fk", "masuk if");
            finish();
            //opening profile activity
            startActivity(new Intent(getApplicationContext(), AdminActivity.class));
        }

        /*inisisalisasi button dan edittext*/
        final EditText etUsername= findViewById(R.id.etUsername);
        final EditText etPassword= findViewById(R.id.etPassword);
        Button btLogin= findViewById(R.id.btLogin);


        /*enter keyboard handler*/
        etUsername.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            etPassword.requestFocus();
                            InputMethodManager imm= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        etPassword.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            /*what u do*/
                            String username= String.valueOf(etUsername.getText());
                            String password= String.valueOf(etPassword.getText());

                             /*check if username and password is filled*/
                            if(username.trim().length()>0 && password.trim().length()>0){
                                if(username.equals("Admin")|| username.equals("admin")){
                                    /*
                                    * Creating user login session
                                    * */
//                                    session.createLoginSession(username);
                                    editor= session.edit();

                                    editor.putString("Name", "Admin");
                                    editor.putBoolean("logged", true);
                                    editor.commit();

                                    /*
                                    * Starting Admin activity
                                    * */
                                    Intent adminIntent= new Intent(getApplicationContext(), AdminActivity.class);
                                    startActivity(adminIntent);
                                    finish();
                                }else if(username.equals("User") || username.equals("user")){
                                    /*
                                    * Creating user login session
                                    * */
//                                    session.createLoginSession(username);
                                    editor= session.edit();

                                    editor.putString("Name", "User");
                                    editor.putBoolean("logged", true);
                                    editor.commit();
                                    /*
                                    * Starting Admin activity
                                    * */
                                    Intent userIntent= new Intent(getApplicationContext(), UserActivity.class);
                                    startActivity(userIntent);
                                    finish();
                                }else{
                                    Toast.makeText(LoginActivity.this, "Username yang anda masukan salah", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(LoginActivity.this, "Mohon lengkapi", Toast.LENGTH_SHORT).show();
                            }

                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });


        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username= String.valueOf(etUsername.getText());
                String password= String.valueOf(etPassword.getText());

                /*check if username and password is filled*/
                if(username.trim().length()>0 && password.trim().length()>0){
                    if(username.equals("Admin")|| username.equals("admin")){
                        /*
                        * Creating user login session
                        * */
//                        session.createLoginSession(username);
                        editor= session.edit();

                        editor.putString("Name", "Admin");
                        editor.putBoolean("logged", true);
                        editor.commit();

                        Toast.makeText(getApplicationContext(), session.getString("Name", ""), Toast.LENGTH_SHORT).show();


                        /*
                        * Starting Admin activity
                        * */
                        Intent adminIntent= new Intent(getApplicationContext(), AdminActivity.class);
                        startActivity(adminIntent);
                        finish();
                    }else if(username.equals("User") || username.equals("user")){
                        /*
                        * Creating user login session
                        * */
//                        session.createLoginSession(username);
                        editor= session.edit();

                        editor.putString("Name", "User");
                        editor.putBoolean("logged", true);
                        editor.commit();
                        /*
                        * Starting Admin activity
                        * */
                        Intent userIntent= new Intent(getApplicationContext(), UserActivity.class);
                        startActivity(userIntent);
                        finish();
                    }else{
                        Toast.makeText(LoginActivity.this, "Username yang anda masukan salah", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(LoginActivity.this, "Mohon lengkapi", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
