package id.tp3.model;

/**
 * Created by FKO on 4/3/2018.
 */

public class Menu {

    private long id;
    private String nama;
    private  String tingkatan;

    public Menu(){}

    public Menu(String nama, String tingkatan) {
        this.nama = nama;
        this.tingkatan = tingkatan;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTingkatan() {
        return tingkatan;
    }

    public void setTingkatan(String tingkatan) {
        this.tingkatan = tingkatan;
    }
}
