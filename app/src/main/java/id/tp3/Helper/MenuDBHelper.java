package id.tp3.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import id.tp3.model.Menu;

/**
 * Created by FKO on 4/3/2018.
 */

public class MenuDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME= "dbMenu.db";
    private static final int DATABASE_VERSION= 1;
    public final String TABLE_NAME= "Menu";
    public final String COLUMN_ID= "ID";
    public final String COLUMN_MENU_NAME= "NAMA";
    public final String COLUMN_MENU_TINGKAT= "TINGKATAN";

//    public static final String TABLE_MENU_CREATE= "CREATE TABLE MENU(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAMA TEXT NOT NULL, " +
//            "TINGKATAN TEXT NOT NULL, IMAGE BLOB NOT NULL)";

    public MenuDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_MENU_NAME + " TEXT , " +
                COLUMN_MENU_TINGKAT + " TEXT )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS MENU");
        this.onCreate(sqLiteDatabase);
    }

    /*create new record*/
    public void saveNewMenu(Menu menu){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values= new ContentValues();
        values.put(COLUMN_MENU_NAME, menu.getNama());
        values.put(COLUMN_MENU_TINGKAT, menu.getTingkatan());
        values.put("IMAGE", "akka");
//        Log.i("FK", menu.getNama());
//        values.put("IMAGE", menu.getImage());

//        insert
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public List<Menu> menuList(String filter){
        String query;
        if(filter.equals("")){
//            regular query
            query= "SELECT * FROM "+TABLE_NAME;
        }else{
//            filter result by filter option provided
            query= "SELECT * FROM "+TABLE_NAME+" ORDER BY"+ filter;
        }

        List<Menu> menuLinkedList= new LinkedList<>();
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor cursor= db.rawQuery(query, null);
        Menu menu;

        if(cursor.moveToFirst()){
            do{
                menu= new Menu();

                menu.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
                menu.setNama(cursor.getString(cursor.getColumnIndex(COLUMN_MENU_NAME)));
                menu.setTingkatan(cursor.getString(cursor.getColumnIndex(COLUMN_MENU_TINGKAT)));
//                menu.setImage(cursor.getString(cursor.getColumnIndex("IMAGE")));

                menuLinkedList.add(menu);
            }while (cursor.moveToNext());
        }
        return menuLinkedList;
    }

    /*if only one record*/
    public Menu getMenu(Long id){
        SQLiteDatabase db= this.getWritableDatabase();
        String query= "SELECT * FROM "+TABLE_NAME+" WHERE ID="+id;
        Cursor cursor= db.rawQuery(query, null);

        Menu receivedMenu= new Menu();
        if(cursor.getCount()> 0){
            cursor.moveToFirst();

            receivedMenu.setNama(cursor.getString(cursor.getColumnIndex("NAMA")));
            receivedMenu.setTingkatan(cursor.getString(cursor.getColumnIndex("TINGKATAN")));
//            receivedMenu.setImage(cursor.getString(cursor.getColumnIndex("IMAGE")));
        }

        return receivedMenu;
    }


    /*delete record*/
    public void deleteMenuRecord(long id, Context context){
        SQLiteDatabase db= this.getWritableDatabase();

        db.execSQL("DELETE FROM MENU WHERE ID='"+id+"'");
        Toast.makeText(context, "Delete Successfully", Toast.LENGTH_SHORT).show();
    }

    /*update record*/
    public void updateMenuREcord(long menuId, Context context, Menu updatedmenu){
        SQLiteDatabase db= this.getWritableDatabase();

        db.execSQL("UPDATE "+TABLE_NAME+" SET NAMA='"+updatedmenu.getNama()+"', TINGKATAN='"+updatedmenu.getTingkatan()+
                "' WHERE ID='"+menuId+"'");

        Toast.makeText(context, "Update Successfully", Toast.LENGTH_SHORT).show();
    }
}
