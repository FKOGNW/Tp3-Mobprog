package id.tp3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import id.tp3.Helper.MenuDBHelper;
import id.tp3.adapter.MenuAdapter;
import id.tp3.adapter.UserMenuAdapter;
import id.tp3.session.SessionManagement;

public class UserActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MenuDBHelper dbHelper;
    private UserMenuAdapter adapter;
    private  String filter= "";

    /*
    * session manager class
    * */
//    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        /*inisialisasi toolbar*/
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar.setNavigationIcon(R.drawable.ic_toolbar);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");

//        session= new SessionManagement(getApplicationContext());
//        session.checkLogin();

         /*initialize recyclerview*/
        mRecyclerView= findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        /*use liner layout manager*/
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        /*populate recyclerview*/
        populateRecyclerView(filter);

//        Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();
    }

    private void populateRecyclerView(String filter) {
        dbHelper= new MenuDBHelper(this);
        adapter= new UserMenuAdapter(dbHelper.menuList(filter), this, mRecyclerView);
        mRecyclerView.setAdapter(adapter);
    }
    /*inflate icon/button yang ada di custom appbar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create2, menu);
        return  true;
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.btnExit:
                Toast.makeText(this, "exit", Toast.LENGTH_SHORT).show();
                SharedPreferences sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
