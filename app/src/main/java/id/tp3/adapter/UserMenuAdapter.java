package id.tp3.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import id.tp3.Helper.MenuDBHelper;
import id.tp3.R;
import id.tp3.UpdateActivity;
import id.tp3.model.Menu;

/**
 * Created by FKO on 4/4/2018.
 */

public class UserMenuAdapter extends RecyclerView.Adapter<UserMenuAdapter.ViewHolder>{
    private List<Menu> mMenuList;
    private Context mContext;
    private RecyclerView mRecyclerV;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNama;
        public TextView tvTingkatan;
        public ImageView image;

        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);
            layout= itemView;

            tvNama= itemView.findViewById(R.id.tvNama);
            tvTingkatan= itemView.findViewById(R.id.tvTingkatan);
            image= itemView.findViewById(R.id.image);
        }
    }

    public void add(int postion, Menu menu){
        mMenuList.add(postion, menu);
        notifyItemInserted(postion);
    }

    public void remove(int position){
        mMenuList.remove(position);
        notifyItemRemoved(position);
    }

    public UserMenuAdapter(List<Menu> myDataset, Context context, RecyclerView recyclerView){
        mMenuList= myDataset;
        mContext= context;
        mRecyclerV= recyclerView;
    }

    @Override
    public UserMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        /*create new view*/
        LayoutInflater infalter= LayoutInflater.from(parent.getContext());

        View v= infalter.inflate(R.layout.recycler_view, parent, false);

        UserMenuAdapter.ViewHolder vh= new UserMenuAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Menu menu= mMenuList.get(position);
        holder.tvNama.setText("Nama : "+ menu.getNama());
        holder.tvTingkatan.setText("Tingkatan : "+menu.getTingkatan());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Data added", Toast.LENGTH_LONG).show();
            }
        });
    }

//    @Override
//    public void onBindViewHolder(MenuAdapter.ViewHolder holder, final int position) {
//        final Menu menu= mMenuList.get(position);
//        holder.tvNama.setText("Nama : "+ menu.getNama());
//        holder.tvTingkatan.setText("Tingkatan : "+menu.getTingkatan());
//        Picasso.with(mContext).load(menu.getImage()).placeholder(R.mipmap.ic_launcher).into(holder.image);


//        /*listen to single view layour click*/
//        holder.layout.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                final AlertDialog.Builder builder= new AlertDialog.Builder(mContext);
//                builder.setTitle("Choose option");
//                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
////                        go to update activity
//                        goToUpdateActivity(menu.getId());
//                    }
//                });
//
//                builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        final AlertDialog.Builder delete= new AlertDialog.Builder(mContext);
//                        delete.setTitle("Are you sure?");
//
//                        delete.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                MenuDBHelper dbHelper= new MenuDBHelper(mContext);
//                                dbHelper.deleteMenuRecord(menu.getId(), mContext);
//
//                                mMenuList.remove(position);
//                                mRecyclerV.removeViewAt(position);
//                                notifyItemRemoved(position);
//                                notifyItemChanged(position, mMenuList.size());
//                                notifyDataSetChanged();
//                            }
//                        });
//
//                        delete.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        });
//
//                        delete.create().show();
//                    }
//                });
//
//                builder.create().show();
//            }
//        });
//    }

//    private void goToUpdateActivity(long menuId){
//        Intent goToUpdate= new Intent(mContext, UpdateActivity.class);
//        goToUpdate.putExtra("USER_ID", menuId);
//        mContext.startActivities(new Intent[]{goToUpdate});
//    }


    @Override
    public int getItemCount() {
        return mMenuList.size();
    }
}
